#include <avr/io.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <avr/interrupt.h>

#define F_CPU 16000000UL //the clock of the cpu (internal is 8 000 000 hz)
#define BRC 1 //((uint64_t)(F_CPU)/((uint64_t)(9600)*(uint64_t)(8))) - (uint64_t)(1) //define the baud rate up to 1M (1 000 000)
//Debug mode for UART 1 or 0
#define DEBUGMODE 1

#include <util/delay.h>

#define BIT_GET(p,m) ((p) & (m)) //if(bit_get(foo, BIT(3)))
#define BIT_SET(p,m) ((p) |= (m)) //bit_set(foo, 0x01); bit_set(foo, BIT(5));
#define BIT_CLEAR(p,m) ((p) &= ~(m)) //bit_clear(foo, BIT(1);
#define BIT_FLIP(p,m) ((p) ^= (m)) //bit_flip(foo, BIT(0));
#define BIT_WRITE(c,p,m) (c ? bit_set(p,m) : bit_clear(p,m)) //bit_write(bit_get(foo, BIT(4)), bar, BIT(0)); (To set or clear a bit based on bit number 4:)
#define BIT(x) (0x01 << (x)) // 0<x<7
#define LONGBIT(x) ((unsigned long)0x00000001 << (x))

//===TX functions
void sw(const char toWrite[], uint8_t useDebugModesw); //print a string on serial
//print a Register or a number (no float), in hex(2) or in decimal(10)
void swn(uint64_t num, int type, int ln, uint8_t useDebugModeswn);
//Take a float and print it on serial
void swf(double numf, uint8_t lnf, uint8_t extra_accur, uint8_t print_it, uint8_t useDebugModeswf);

//Serial Write Hex
//Use like: swh("2A",0,0); to send a char
//Or like: swh(" ",0x2A,1); to send a Hex
void swh(const char toWriteChar[], uint8_t toWriteInt, uint8_t useInt);

//===RX functions
void readUntill(char c_rx);

//======================TX START======================
uint8_t len, k=0; //unsigned 8 bit integer
char str_floatx[10];
//======================TX END=======================

//======================RX START=======================
uint8_t rxBufferuint[6];
char udr0 = '\0', rx_stop='\0'; /*  NULL = \0  */
uint8_t rxReadPos = 0, rxWritePos = 0, rxComplete=0;
uint8_t rxProcessing = 0; //which processing array block are we
uint8_t rxBufferuintFlush = 0;//helps when rxwritePos is 0, so that readPos wont think its left behind.
/*
rxComplete is set when a receive is complete.
*/
//======================RX END=======================

//UART init functions
void ringBufferShift();//shifts the buffer to read the next user input
void init_rxtx(); //initializes only rxtx functionality

//=============FreeRam=========================
void RemainingRam();

//=====================EEPROM functions=====================
void EEPROM_write(unsigned int uiAddress, uint8_t ucData);
unsigned int EEPROM_read(unsigned int uiAddress, uint8_t printHex);

//TWI
//inits TWI with a bit rate automatically
void TWI_Init(uint32_t freqencyHz); //desired freq in Hz
//waits until the given status code appears
uint8_t TWI_Check_Status_Code(uint8_t statusCodeToCheck);
//Sends a start signal to TWI
uint8_t TWI_Start(uint8_t CheckStartStatus);
//sends data to twi
uint8_t TWI_Write(uint8_t Data, uint8_t CheckWriteStatus);
//Read from the TWI, Read result is in TWDR.
uint8_t TWI_Read(uint8_t CheckReadStatus);
//Stops the TWI
void TWI_Stop();
/*
prints dialog explaining the return/checked response code of the TWI.
=0 Command applied successfully, no code to check was given.
=1 ERROR: timer to apply command exceeded. Do we use pull-up resistors on TWI line?
=2 success reply from TWI, command as expected.
else:
TWI response not what expected. Got: ...
*/
void TWI_print_status(const char message[], uint8_t checkResponse);
#define TWI_SEND_DATACOUNT 1
#define TWI_SEND_REGADDR 2
#define TWI_SEND_DATA 3
#define TWI_STOP_NEXT_REG 4
#define TWI_STOP 5
#define TWI_LOOP 6

#define READ_MODE 7
#define WRITE_MODE 8
#define START_IN_READ 9
#define TWI_SLAVE_READ 10
#define TWI_MASTER_READS_BYTECOUNT 11

volatile uint8_t twi_next=0;//what will twi do next
uint8_t USB2517Config[256];//all the registers data to be written
uint8_t USB2517_regpos=0;//what reg pos to write next.
uint8_t twi_mode=WRITE_MODE;

uint8_t USB2517RXData[260];
uint16_t rxDataPos = 0;

int main(void)
{
	init_rxtx();
	sw("Waiting...\n\r",1);
	_delay_ms(5000);
	sw("Start!\n\r",1);
	
	USB2517Config[0]=0x24;
	USB2517Config[1]=0x04;
	USB2517Config[2]=0x17;
	USB2517Config[3]=0x25;
	USB2517Config[4]=0x00;
	USB2517Config[5]=0x00;
	USB2517Config[6]=0xBB;//Changed from original 0x9B
	USB2517Config[7]=0x20;
	USB2517Config[8]=0x00;
	USB2517Config[9]=0x00;
	USB2517Config[10]=0x00;//0A
	USB2517Config[11]=0x00;//0B
	USB2517Config[12]=0x01;//0C
	USB2517Config[13]=0x32;//0D
	USB2517Config[14]=0x01;//0E
	USB2517Config[15]=0x32;//0F
	USB2517Config[16]=0x32;//10
	
	uint16_t i;//fill the rest of the table with 0s
	for (i=17; i<=254; i++)
	{
		USB2517Config[i]=0x00;
	}
	USB2517Config[255]=0B00000101;
	sw("Array filled\n\r",1);
	//print all table's contents.
	for (i=0; i<=255; i++)
	{
		swn(USB2517Config[i],16,1,1);
	}
	
	TWI_Init(100000);//in Hz
	BIT_SET(SREG, BIT(7));//Interrupt bit in SREG set
	//master
	TWAR = 0B10100001;//Device�s Own Master Address is 0x50 (0001000X), responds to general call(XXXXXX1)
	TWCR = 0B11100101;//start I2C master
/*	
	uint8_t tempvar = TWCR;
	sw("TWCR is:",1);
	swn(tempvar,2,1,1);
	sw("\n\r",1);
*/	
	
		
	
	
    /* Replace with your application code */
    while (1) 
    {		
		if (twi_next == TWI_STOP)
		{
			static uint8_t twStatus;
			twStatus = TWSR;//read the status
					
			BIT_CLEAR(twStatus, BIT(0));//clear prescaler values
			BIT_CLEAR(twStatus, BIT(1));
					
			_delay_ms(1000);
			sw("TWSR status code is ",1);
			swn(twStatus,16,1,1);
			sw("\r",1);	
		}

    }
}

//twi interrupt
ISR(TWI_vect)
{	
	static uint8_t twStatus;
	twStatus = TWSR;//read the status
	
	BIT_CLEAR(twStatus, BIT(0));//clear prescaler values
	BIT_CLEAR(twStatus, BIT(1));
	
	sw("TWSR=",1);
	swn(twStatus,16,1,1);
	sw(", ",1);	
	
	static uint8_t twcr_config;//placeholder for TWCR register
	twcr_config=0;//clear the register, perform initial values.
	BIT_SET(twcr_config, BIT(7));//Clear TWINT
	BIT_SET(twcr_config, BIT(6));//TWEA enable
	BIT_SET(twcr_config, BIT(2));//TWEN enable
	BIT_SET(twcr_config, BIT(0));//TWIE enable
	
	if (twStatus == 0x08)//start transmitted.
	{
		sw("start OK.",1);
		//SLA+W will be transmitted.

		TWDR = 0B01011000;//slave address, 0=write.
		twi_next=TWI_SEND_REGADDR;
		
		BIT_CLEAR(twcr_config, BIT(5));//Clear TWSTA
		BIT_CLEAR(twcr_config, BIT(4));//Clear TWSTO
	}
	else if (twStatus == 0x10)
	{
		sw("repeated Start\n\r",1);
		
		TWDR = 0B01011001;//slave address, 1=read.
		twi_next = TWI_SEND_DATACOUNT;		
		
		BIT_CLEAR(twcr_config, BIT(5));//Clear TWSTA
		BIT_CLEAR(twcr_config, BIT(4));//Clear TWSTO		
	}
	else if (twStatus == 0x18)
	{
		sw("SLA+W OK, ACK OK.",1);
				
		if (twi_next == TWI_SEND_REGADDR)
		{
			sw("sending reg Address ",1);
			swn(USB2517_regpos,16,1,1);
			TWDR = USB2517_regpos;//the increase of regpos is done after data transmission
			
			if (twi_mode == WRITE_MODE)
			{
				twi_next = TWI_SEND_DATACOUNT;		
			}
			else if (twi_mode == READ_MODE)
			{
				twi_next = START_IN_READ; //(repeated START)
			}
			BIT_CLEAR(twcr_config, BIT(5));//Clear TWSTA
			BIT_CLEAR(twcr_config, BIT(4));//Clear TWSTO
		}	
		
	}
	else if (twStatus == 0x28)
	{
		sw("Data OUT OK, ACK OK.",1);
		
		BIT_CLEAR(twcr_config, BIT(5));//Clear TWSTA
		BIT_CLEAR(twcr_config, BIT(4));//Clear TWSTO
				
		if (twi_next == TWI_SEND_DATACOUNT)
		{
			sw("sending datacount.",1);
			TWDR = 0x01;//1 data will be transmited
			twi_next = TWI_SEND_DATA;
		}
		else if (twi_next == TWI_SEND_DATA)
		{
			sw("sending Data:",1);
			swn(USB2517Config[USB2517_regpos],16,1,1);
			
			TWDR = USB2517Config[USB2517_regpos];
			USB2517_regpos++;//increase for the next pos
			if (USB2517_regpos == 0)//if regpos just send the last register
			{
				twi_next = TWI_STOP;//finished transmitting data
				twi_mode = READ_MODE;//switch to read mode.
			}
			else
			{
				twi_next = TWI_STOP_NEXT_REG;//stop and then go to next reg
			}
			
		}
		else if (twi_next == TWI_STOP_NEXT_REG)
		{
			sw("will stop but start next reg\n\r",1);
			BIT_SET(twcr_config, BIT(4));//Set STOP
			BIT_SET(twcr_config, BIT(5));//Set TWSTA
		}
		else if (twi_next == TWI_STOP)
		{
			sw("will stop\n\r",1);
			BIT_SET(twcr_config, BIT(4));//Set STOP
			twi_next=TWI_LOOP;
		}
		else if (twi_next == START_IN_READ)
		{
			BIT_SET(twcr_config, BIT(5));//Clear TWSTA
			BIT_CLEAR(twcr_config, BIT(4));//Clear TWSTO
			
			twi_next = TWI_SLAVE_READ;
		}
		else if (twi_next == TWI_LOOP)
		{
			sw("loop entered\n\r",1);
			while (1)
			{
			}
		}
	}
	else if (twStatus == 0x30)
	{
		sw("SLA+W transmitted, NACK received\n\r",1);
		while (1)
		{
		}
	}
	else if (twStatus == 0x40)
	{
		sw("SLA+R OK, ACK OK.",1);
		twi_next = TWI_MASTER_READS_BYTECOUNT;//master will read the data
		BIT_SET(twcr_config, BIT(6));//TWEA set
		BIT_CLEAR(twcr_config, BIT(5));//TWSTA clear
		BIT_CLEAR(twcr_config, BIT(4));//TWSTO clear
	}
	else if (twStatus == 0x50)
	{
		sw("Data IN OK, ACK OK=",1);
		USB2517RXData[rxDataPos]= TWDR;//pos 0 contains No of data
		swn(USB2517RXData[rxDataPos],16,1,1);
		rxDataPos++;//increase for next RX pos
		
		if ( (rxDataPos -1) == USB2517RXData[0] )//if we reached No of data
		{
			sw("STOP now",1);
			twi_next = TWI_STOP;
			BIT_CLEAR(twcr_config, BIT(6));//TWEA clear
			BIT_CLEAR(twcr_config, BIT(5));//TWSTA clear
			BIT_SET(twcr_config, BIT(4));//TWSTO set			
		}
		else
		{//data byte will received again
			BIT_SET(twcr_config, BIT(6));//TWEA set
			BIT_CLEAR(twcr_config, BIT(5));//TWSTA clear
			BIT_CLEAR(twcr_config, BIT(4));//TWSTO clear
		}
	}
	
	TWCR = twcr_config;	
}

//inits TWI with a bit rate automatically
void TWI_Init(uint32_t frequencyHz) //desired freq in Hz
{
	uint32_t twbr_result;//calculated twdr for given prescaler
	uint16_t prescaler=1;//initial prescaler

	do
	{
		twbr_result = (F_CPU - (16*frequencyHz) );
		
		twbr_result = twbr_result / ( 2 * prescaler * frequencyHz);//function to find twbr
		
		prescaler = prescaler*4;//next prescaler
		
	} while ( (twbr_result > 255) && (prescaler/4 < 64) );//prescaler cant be >64, twbr cant be > 255
	
	prescaler = prescaler/4;//restore the last prescaler
	
	
	//if twbr failed or prescaler is >64
	if( (twbr_result > 255) || (prescaler!=1 && prescaler!=4 && prescaler!=16 && prescaler!=64) )
	{
		sw("TWI ERR\n",1);
	}
	else//function succeeded
	{
		TWBR = twbr_result;//save twbr
		
		//save prescaler
		if(prescaler == 1)
		{
			BIT_CLEAR(TWSR,BIT(1)); //TWPS1=0;
			BIT_CLEAR(TWSR,BIT(0)); //TWPS0=0;
		}
		else if (prescaler == 4)
		{
			BIT_CLEAR(TWSR,BIT(1)); //TWPS1=0;
			BIT_SET(TWSR,BIT(0)); //TWPS0=1;
		}
		else if (prescaler == 16)
		{
			BIT_SET(TWSR,BIT(1)); //TWPS1=1;
			BIT_CLEAR(TWSR,BIT(0)); //TWPS0=0;
		}
		else if (prescaler == 64)
		{
			BIT_SET(TWSR,BIT(1)); //TWPS1=1;
			BIT_SET(TWSR,BIT(0)); //TWPS0=1;
		}
		
		/*sw("twbr is: ",1);
		swn(TWBR,10,1,1);
		sw("\r",1);
		
		sw("prescaler is: ",1);
		swn(prescaler,10,1,1);
		sw("\r",1);
		*/
	}
}

//============================EEPROM FUNCTIONS============================

//0<=Address<=1023
void EEPROM_write(unsigned int uiAddress, uint8_t ucData)
{
	/* Wait for completion of previous write/read */
	while(EECR & (1<<EEPE));
	/* Set up address and Data Registers */
	EEAR = uiAddress;
	EEDR = ucData;
	/* Write logical one to EEMPE */
	EECR |= (1<<EEMPE);
	/* Start eeprom write by setting EEPE */
	EECR |= (1<<EEPE);
}

unsigned int EEPROM_read(unsigned int uiAddress, uint8_t printHex)
{
	uint8_t eeprom_ret;
	/* Wait for completion of previous write/read */
	while(EECR & (1<<EEPE));
	/* Set up address Register */
	EEAR = uiAddress;

	/* Start eeprom read by setting EEPE */
	EECR |= (1<<EERE);
	
	while(EECR & (1<<EEPE));//wait for the read to finish
	
	eeprom_ret = EEDR;
	
	if(printHex)
	{
		swh(" ", eeprom_ret, 1);
	}
	
	return eeprom_ret;
}

//Init USB communication
void init_rxtx()
{
	//==================================TX START====================================
	BIT_SET(UCSR0A, BIT(1));//U2Xn = 1 (double speed)
	UBRR0H = (BRC >> 8); //Put BRC to UBRR0H and move it right 8 bits.
	UBRR0L = BRC;
	UCSR0B = (1 << TXEN0); //Trans enable
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); //8 BIT data frame

	//ENABLE interrupts
	BIT_SET(SREG, BIT(7));//I=1 Global interrupt enabled
	//==================================TX END=====================================
	//================================== RX START===========================================
	UCSR0B |= (1 << RXEN0) | (1 << RXCIE0); //RX enable and Receive complete interrupt enable
	//================================== RX END=============================================
}

void sw(const char toWrite[], uint8_t useDebugModesw)
{
	if(useDebugModesw)//if we want to use debug mode
	{
		if(DEBUGMODE)//if the debug mode is active
		{
			len = strlen(toWrite); //take size of characters to be printed
			k=0;	//initialize k
			//UDR0=0; //make sure UDR0 is 0
			
			while (k<len) //while i is less than the total length of the characters to be printed
			{
				if (  ((UCSR0A & 0B00100000) == 0B00100000)  ){ //if UDRE0 is 1 (aka UDR0 is ready to send)
					UDR0 = toWrite[k]; //put the next character to be sent (now, UDRE0 is 0)
					k++;		//increase the position, and wait until UDRE0 is 1 again
				}
			}
			//udr0 = '\0';
		}
		else//if the debug mode is inactive
		{
			//do nothing
		}
	}
	else//if we do not care about debug mode, print
	{
		len = strlen(toWrite); //take size of characters to be printed
		k=0;	//initialize k
		//UDR0=0; //make sure UDR0 is 0
		
		while (k<len) //while i is less than the total length of the characters to be printed
		{
			if (  ((UCSR0A & 0B00100000) == 0B00100000)  ){ //if UDRE0 is 1 (aka UDR0 is ready to send)
				UDR0 = toWrite[k]; //put the next character to be sent (now, UDRE0 is 0)
				k++;		//increase the position, and wait until UDRE0 is 1 again
			}
		}
		//udr0 = '\0';
	}
}

//Serial Write Hex
//Use like: swh("2A",0,0); to send a char
//Or like: swh(" ",0x2A,1); to send a Hex
void swh(const char toWriteChar[], uint8_t toWriteInt, uint8_t useInt)
{
	
	while (  ((UCSR0A & 0B00100000) != 0B00100000)  )//if UDRE0 is 1 (aka UDR0 is ready to send)
	{
		//Wait
	}

	//if we want to send a uint_8t
	if(useInt)
	{
		UDR0 = toWriteInt;
	}
	else
	{
		UDR0 = strtol(toWriteChar, 0, 16); //Transform char to hex and put the next character to be sent
	}
}

void swn(uint64_t num, int type, int ln, uint8_t useDebugModeswn) //take an int and print it on serial
{
	static char str_intx[50];//declare a string of 50 characters
	
	ltoa(num, str_intx, type);//convert from long to char and save it on str
	
	sw(str_intx, useDebugModeswn); //serial write str
	
	if(ln == 1) //if we want a new line,
	{
		sw("\n\r",useDebugModeswn);
	}
}

//Input: float number, output: print float number to serial, OR only return the float as string.
//Take a float and print it on serial
void swf(double numf, uint8_t lnf, uint8_t extra_accur, uint8_t print_it, uint8_t useDebugModeswf) 
{
	
	if(extra_accur == 0 || extra_accur == 1)//works with very good accuracy
	{
		/*
		* Needs library linking. Below tutorial is for Microchip studio.
		* Project properties toolchain..XC8 Linker/General -> Tick 'Use vprintf library(-Wl,-u,vprintf)
		* And: XC8 Linker/Miscallaneous insert at 'Other linker flags' this: -lprintf_flt
		* https://startingelectronics.org/articles/atmel-AVR-8-bit/print-float-atmel-studio-7/
		*/
		sprintf(str_floatx, "%f", numf); //needs library linking or itll print something like ' ?); '
	}

	if(print_it)
	{
		sw(str_floatx,useDebugModeswf);	
		
		if(lnf == 1) //if we want a new line,
		{
			sw("\n\r",useDebugModeswf);
		}
	}
}

//==================================TX END=====================================

//the result (input) 
void readUntill(char c_rx)
{
	rxWritePos = 0;//begin writing in the buffer from pos 0
	rx_stop = c_rx; //interrupt will use the global var rx_stop to stop reading the string
}


ISR(USART_RX_vect)
{
		rxBufferuint[rxWritePos] = UDR0;//save incoming char
		rxWritePos++;
		
		if (rxWritePos == 5)//5-pos buffer
		{
			rxWritePos=0;//reset buffer
		}
		
		if(rxWritePos == 0)
		{//if we are back at 0
			rxBufferuintFlush = 1;//buffer flushed and overflowed
		}
		rxComplete = 0B00000001;
}

//==================================RX END==================================

/*
Shifts the buffer pointer to the next array block, if there is any available next block. This function does not pause the
Code. If we are waiting for an incoming package to arrive, we should run this function inside a loop, by storing the rxProcessing
value to another variable before the loop, and wait untill this variable changes. This will mean that the buffer moved aka
a new package arrived.
*/
void ringBufferShift()
{
	if( (rxProcessing < (rxWritePos-(1))) || ( (rxBufferuintFlush == 1U) && ( (uint8_t)(rxProcessing + 1U) != ((uint8_t)rxWritePos) ) ) )
	{//load the next block of the buffer
		
		++rxProcessing;
		
		if(rxProcessing == 0)
		{//if we are back from where we started
			rxBufferuintFlush = 0;//reset the flush flag
		}
	}
}

//see how much free Sram is left https://stackoverflow.com/a/7190848/13294095
void RemainingRam()
{	
	static uint16_t freeram;
	
	extern int __heap_start, *__brkval;
	static int v;
	freeram = (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
	
	sw("Ram=",1);
	swn(freeram,10,1,1);
}